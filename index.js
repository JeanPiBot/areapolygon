// n = 1 => 1
// n = 2 => 1 + (1 * 4)
// n = 3 => 1 + (1 * 4) + (2 * 4)
// n = 4 => 1 + (1 * 4) + (2 * 4) + (3 * 4)

const ShapeArea = (n) => {
    if (n === 1) return 1
    return  ShapeArea(n - 1) + ((n -1) * 4)
}

console.log(ShapeArea(2))
console.log(ShapeArea(3))